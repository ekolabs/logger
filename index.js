'use strict';

const fs = require('fs');
const when = require('when');
const config = require('./config');
const path = require('path');

// all known reporters, including the base class
let reporterModules = fs.readdirSync(path.resolve(__dirname, 'reporters'));
let reporterConstructors = {};
reporterModules.map(m => { m = m.replace('.js',''); reporterConstructors[m] = require(`./reporters/${m}`); });

// the log levels.  Ordered by increasing urgency
const logLevels = ['trace', 'debug', 'info', 'log', 'warn', 'error'];

/**
 * The Eko Logger service.
 *
 * @class EkoLogger
 * @constructor
 * @param {Object} args Includes properties which are defauls for this instance, such as reporter and service_id
 * @param {Object} options for reporters
 * @param {String} options.environment Should be 'production' or 'staging'
 * @param {Object} options.reporters A collection of reporters and each reporter name maps to an options object which are used to initialize that specific reporter
 *
 * Example:
 *
 *     args = {
 *        reporter: 'encoding',
 *        service_id: '100'
 *      };
 *
 *      options = {
 *           environment: 'staging',
 *           reporters: {
 *              file: {
 *                logFileName: 'file_with_logs.txt',
 *                minLevel: 'debug'
 *              },
 *              sentry: {
 *                sentryDSN: 'sentry DSN string',
 *                minLevel: 'error'
 *              },
 *              firehose: {
 *                AWSOptions: {
 *                  'DeliveryStream': 'aaa',
 *                  'region': 'bbb',
 *                  'credentials': 'instance of AWS.Credentials',
 *                  'accessKeyId': 'AAAA',
 *                  'secretAccessKey': 'BBBB'
 *                }
 *              }
 *           }
 *      };
 *
 * Each reporter can be supplied a 'minLevel' attribute.  This attribute will
 * limit which events are transmitted to this reporter.  The levels are, in ascending
 * order: debug, info, log, warn, error.  So if minLevel is set to 'log', then all
 * events at the debug and info level will not be transmitted.
 */
var EkoLogger = function (args, options) {

    // Make sure options is object-like
    this.args = args || {};
    this.reporters = [];
    this.options = options || {};
    this.options.reporters = this.options.reporters || {};
    this.options.environment = this.options.environment || 'staging';

    // Validate
    if (!args || (!args.service_id && !args.serviceID) || !args.reporter) {
        throw new Error('Invalid options in logger.  Must include service_id and reporter properties.');
    }

    // create an instance for each reporter, supplying the reporter-specific options
    for (var reporterName in this.options.reporters) {
        let options = this.options.reporters[reporterName];
        options.environment = this.options.environment;
        this.reporters[reporterName] = new reporterConstructors[reporterName](options);
    }
};

/**
 * The logging functions.
 *
 * Call these functions to log a message
 * NOTE: syntactic sugar functions (trace, debug, info, warn, error) will log
 * the message at the corresponding log level
 *
 * @method warn
 * @param {String} message The message string to log
 * @param {Object} args Attributes for reporter and service_id that may override the defaults provided on the constructor
 * @return {Object} a Promises/A+ promise.
 */
logLevels.map(function (level) {
    EkoLogger.prototype[level] = function (msg, args) {
        EkoLogger.prototype.reportLogLevel.call(this, level, msg, args);
    };
});

// generic report log
EkoLogger.prototype.reportLog = EkoLogger.prototype[config.DEFAULT_LOG_LEVEL];
EkoLogger.prototype.reportLogLevel = function (log_level, msg, args) {
    let data = {
        log: msg,
        task_time: (new Date()).getTime(),
        log_level: log_level
    };
    Object.assign(data, this.args, args);

    if (!data.task_id && !data.taskID) {
        throw new Error('Logger: No taskID to report to.');
    }

    this.handleOverflows(data);

    let promises = [];
    for (let r in this.reporters) {
        var reporter = this.reporters[r];

        // check if the reporter accepts this log level
        if (levelTooLow(this.options.reporters[r], log_level)) {
            continue;
        }

        try {
            promises.push(reporter.log(data));
        } catch (e) {
            if (this.options.verbose) {
                console.log(`reporter ${reporter.name} failed ${e.toString()}`);
            }
        }
    }

    return when.all(promises);
};

// check if this reporter should process events for this level
function levelTooLow(reporterOptions, currentLevel) {
    return reporterOptions &&
        reporterOptions.minLevel &&
        (logLevels.indexOf(currentLevel) < logLevels.indexOf(reporterOptions.minLevel));
}

// put unrecognized attributes in the 'overflows' attribute inside a JSON-encoded string.
// This functionality simplifies reporters such as Firehose which require
// a strict set of primary attributes.
EkoLogger.prototype.handleOverflows = function (obj) {
    obj.overflows = obj.overflows || {};
    for (var key in obj) {
        if (config.DB_ATTRIBUTES.indexOf(key) < 0) {
            obj.overflows[key] = obj[key];
            delete obj[key];
        }
    }
    obj.overflows = JSON.stringify(obj.overflows);
};

module.exports = EkoLogger;
