# Eko Logger README #

The Eko Logger is a library which provides a set of logging functions (trace, debug, info, warn, error).
Eko services such as encoding and publishing use the Eko Logger to record various logging events, for purposes of auditing and debugging.  In those cases, the log messages are transmitted to the Eko back-end servers.

### How do I get set up? ###

`npm install --save @ekolabs/logger`

### API ###

Require the class constructor and create a new instance.  On the constructor invocation you can pass arguments and options:

Arguments object:

* serviceID - the service id, for example 'ENCODING'.  Mandatory in constructor
* reporter - the reporter, for example 'encoder-cli'.  Mandatory in constructor


Options object:

* environment - 'staging' or 'production'
* reporters - an object with an entry for each reporting method

Full API docs are available by running yuidocs.

### Usage example ###

```
var EkoLogger = require('@helloeko/logger');

var loggerArgs = {
    reporter: 'niftyService',
    serviceID: 'serviceABC'
};

var loggerOptions = {
    environment: 'staging',
    reporters: {
        file: {
            logFileName: 'logs_data.txt',
            sync: true,
            minLevel: 'debug'
        },
        sentry: {
            sentryDSN: 'https://123abc@app.getsentry.com/12345',
            minLevel: 'warn'
        },
        firehose: {
            AWSOptions: {
                DeliveryStream: 'kinesis_stream_100'
            }
        }
    }
};

var logger = new EkoLogger(loggerArgs, loggerOptions);


logger.trace('omg what a lovely readme!');
logger.debug('someone just read the readme');
logger.info('you should check out this readme');
logger.warn('readme is rather boring', {taskID: 'ca13caab-f7ae-4534-bd6b-99e0555a4f6e'});
logger.error('readme is too long', {taskID: 'ca13caab-f7ae-4534-bd6b-99e0555a4f6e'});
```
