const fs = require('fs');
const when = require('when');
const Reporter = require('./reporter');

class FileReporter extends Reporter {

    constructor(options) {
        super();
        this.options = options || {};
        this.name = 'file';
        if (!options.logFileName) {
            options.logFileName = 'ekologs';
        }
        this.logFileName = options.logFileName;
    }

    log(record) {
        var text = `[${record.log_level}]: ${JSON.stringify(record)}\n`;
        if (this.options.sync) {
            fs.appendFileSync(this.logFileName, text);
            return when.resolve();
        } else {
            return when.promise(function (resolve, reject) {
                fs.appendFile(this.logFileName, text, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });

        }

    }
}

module.exports = FileReporter;
