const when = require('when');

/**
 * The base class for reporters.
 *
 * @class Reporter
 */
class Reporter {

    /**
     * The generic log function.  Each subclass will implement its own
     * logging logic.
     *
     * @method log
     * @param {Object} record The record to be logged.
     * @param {String} record.log_level The log level of this record
     * @param {String} record.log The string message to be logged
     * @param {String} record.reporter The component that reported the record
     * @param {String} record.service_id The service that was used
     * @return {undefined} Returns nothing.  Exceptions will be ignored!
     */
    log(record) {
        console.log(`[${record.log_level}]: ${JSON.stringify(record)}\n`);
        return when.resolve();
    }

}

module.exports = Reporter;
