const raven = require('raven');
const when = require('when');
const Reporter = require('./reporter');

class SentryReporter extends Reporter {

    constructor(options) {
        super();
        this.options = options || {};
        this.name = 'sentry';
        if (!options.sentryDSN) {
            throw new Error('SentryReporter initialized but no sentryDSN provided');
        }
        console.log(`starting raven with ${this.options.sentryDSN}`);
        this.ravenClient = new raven.Client(this.options.sentryDSN);
        this.ravenClient.captureMessage('hello hello eko');
    }

    log(record) {
        if (record.log_level === 'error') {
            this.ravenClient.captureMessage(record.log, { extra: record });
        }
        return when.resolve();
    }

}

module.exports = SentryReporter;
