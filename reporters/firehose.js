const when = require('when');
const AWS = require('aws-sdk');
const config = require('../config');
const Reporter = require('./reporter');


class FirehoseReporter extends Reporter {

    constructor(options) {

        super();
        this.options = options;
        options.AWSOptions.region = options.AWSOptions.region || config.DEFAULT_REGION;
        options.AWSOptions.DeliveryStream = options.AWSOptions.DeliveryStream || config.DEFAULT_STREAM[options.environment];
        this.name = 'firehose';

        // Initialize AWS
        let AWSOptions = {};

        // If region was provided then use it
        if (options.AWSOptions.region) {
            AWSOptions.region = options.AWSOptions.region;
        }

        // Legacy: support accessKey + secret
        if (options.AWSOptions.accessKeyId && options.AWSOptions.secretAccessKey) {
            options.AWSCredentials = new AWS.Credentials(options.AWSOptions.accessKeyId, options.AWSOptions.secretAccessKey);
        }

        // Use credentials if provided.  If no credentials are available,
        // the AWS library will try to obtain credentials from the system.
        if (options.AWSOptions.credentials) {
            AWSOptions.credentials = options.AWSOptions.credentials;
        }

        // Get Firehose service
        this.firehose = new AWS.Firehose(options.AWSOptions);
    }

    log(record) {

        // convert from camelCase to underscore_case for SQL compatibility on Redshift
        // for this special list
        var autoConvert = {serviceID: 'service_id', taskID: 'task_id', taskParentID: 'taskparent_id'};
        for (var k in autoConvert) {
            if (record.hasOwnProperty(k)) {
                record[autoConvert[k]] = record[k];
                delete record[k];
            }
        }

        let that = this;
        return when.promise((resolve, reject) => {
            let encodedData = JSON.stringify(record);
            that.firehose.putRecord({
                    DeliveryStreamName: that.options.AWSOptions.DeliveryStream,
                    Record: {
                        Data: encodedData
                    }
                },
                (err, data) => {
                    if (err) {
                        reject('EkoLogger error: ' + err);
                    } else {
                        resolve(data);
                    }
                });
        });
    }
}

module.exports = FirehoseReporter;
