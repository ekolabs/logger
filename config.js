module.exports = {
    DEFAULT_STREAM: {
        'production': 'logger_stream',
        'staging': 'logger_stream_staging'
    },
    DEFAULT_REGION: 'us-east-1',
    DEFAULT_LOG_LEVEL: 'log',
    DEFAULT_ENVIRONMENT: 'staging',
    DB_ATTRIBUTES: ['service_id', 'serviceID', 'task_id', 'taskID', 'taskparent_id', 'taskParentID', 'task_type', 'reporter', 'log', 'log_level', 'task_time']
};
