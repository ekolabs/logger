var EkoLogger = require('./index.js');
var fs = require('fs');

// constants
var FILE_NAME = 'test_the_logger.txt';
var testid = 0;
var expectedResults = [3, 3, 1, 0];

// pre-cooked arguments and options for each test
var args = [{
    serviceID: 'ENCODING',
    reporter: 'shula',
    taskID: 'task1'
}, {
    serviceID: '12',
    reporter: 'shula',
    taskID: 'task2'
}, {
    serviceID: '12',
    reporter: 'shula',
    taskID: 'task3'
}, {
    serviceID: '1',
    reporter: 'shula',
    taskID: 'task4'
}];

var options = [{
    environment: 'staging',
    reporters: {
        file: {
            logFileName: FILE_NAME,
            sync: true
        }
    }
}, {
    environment: 'staging',
    reporters: {
        file: {
            logFileName: FILE_NAME,
            sync: true
        }
    }
}, {
    environment: 'staging',
    reporters: {
        file: {
            logFileName: FILE_NAME,
            sync: true,
            minLevel: 'error'
        }
    }
}, {

}];

// remove leftover file
try {fs.unlinkSync(FILE_NAME);} catch(e) {}
var loggers = [];

// add sentry tester to test #2
if (process.env.SENTRY_TESTER) {
    options[1].reporters.sentry = {
        sentryDSN: process.env.SENTRY_TESTER,
        minLevel: 'warn'
    };
}

// add Firehose tester to test #3
if (process.env.DELIVERY_STREAM) {
    options[2].reporters.firehose = {
        AWSOptions: {
            DeliveryStream: process.env.DELIVERY_STREAM
        }
    };
}

// create the loggers
for (testid = 0; testid < 4; testid++) {
    loggers.push(new EkoLogger(args[testid], options[testid]));
}

// submit a log, warn and error on each logger
for (testid = 0; testid < 4; testid++) {
    loggers[testid].log(`<logger${testid}> log`);
    loggers[testid].warn(`<logger${testid}> warn`);
    loggers[testid].error(`<logger${testid}> error`);
}

// check if the file logger put the logs in the expected file
// compare with the expected results.
var testFile = fs.readFileSync(FILE_NAME);
var outputString = testFile.toString();
for (testid = 0; testid < 4; testid++) {
    var matches = outputString.match(new RegExp('logger' + testid, 'g'));
    matches = matches ? matches.length : 0;
    if (matches !== expectedResults[testid]) {
        throw new Error('test failed.  testid = ' + testid);
    }
}
console.log('tests completed successfully');
